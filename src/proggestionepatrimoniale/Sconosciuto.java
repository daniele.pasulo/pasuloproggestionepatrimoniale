package proggestionepatrimoniale;

public class Sconosciuto extends Edificio {
    public Sconosciuto(String via, String città, String stato, int zip, int letti, int bagni, int superficie, String dataVendita, int prezzo, double latitudine, double longitudine) {
        super(via, città, stato, zip, letti, bagni, superficie, "Unknown", dataVendita, prezzo, latitudine, longitudine);
    }

    public Sconosciuto(Sconosciuto e) {
        super(e);
    }

    @Override
    public Sconosciuto clone() {
        return new Sconosciuto(this);
    }
}
