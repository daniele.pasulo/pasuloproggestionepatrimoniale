package proggestionepatrimoniale;

public class Multifamiliare extends Edificio {

    public Multifamiliare(String via, String città, String stato, int zip, int letti, int bagni, int superficie, String dataVendita, int prezzo, double latitudine, double longitudine) {
        super(via, città, stato, zip, letti, bagni, superficie, "Multi-Family", dataVendita, prezzo, latitudine, longitudine);
    }

    public Multifamiliare(Multifamiliare m) {
        super(m);
    }

    @Override
    public Multifamiliare clone() {
        return new Multifamiliare(this);
    }
}
