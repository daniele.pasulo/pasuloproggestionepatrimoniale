package proggestionepatrimoniale;

import java.util.Scanner;

public class ProgGestionePatrimoniale {

    static GestionePatrimoniale g = new GestionePatrimoniale();
    public static void main(String[] args) {
        System.out.print("Salvare(s) o caricare(l): ");
        Scanner in = new Scanner(System.in).useDelimiter(System.lineSeparator());
        String scelta = in.nextLine();
        System.out.print("Percorso del file: ");
        String filename = in.nextLine();
        switch (scelta) {
            case "s": {
                save(filename);
                break;
            }

            case "l": {
                load(filename);
            }
        }
        in.close();
    }

    public static void save(String filename) {
        g.loadCSV("Sacramentorealestatetransactions.csv");
        Edificio e = new Residenziale("Corso Verona 7", "Rovereto", "Italia", 38068, 5, 3, 180, "20/07/2020", 150000, 124.158, -87.895);
        g.add(e);
        g.stampa();
        System.out.println(g.calcolaSommeTotali());
        g.remove(e);
        g.stampa();
        System.out.println(g.calcolaSommeTotali());

        g.save(filename);
    }

    public static void load(String filename) {
        g.load(filename);
        System.out.println(g.calcolaSommeTotali());
        g.stampa();
    }
}
