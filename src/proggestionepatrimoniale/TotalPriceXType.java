package proggestionepatrimoniale;

import java.util.ArrayList;

public class TotalPriceXType {

    private ArrayList<TotalPrice> totalPrices;

    public TotalPriceXType() {
        totalPrices = new ArrayList<>();
        totalPrices.add(new TotalPrice("Residential"));
        totalPrices.add(new TotalPrice("Multi-Family"));
        totalPrices.add(new TotalPrice("Condo"));
        totalPrices.add(new TotalPrice("Unknown"));
    }

    public void add(String type, int price) {
        int index = totalPrices.indexOf(new TotalPrice(type));
        if (index >= 0 && index < totalPrices.size()) {
            totalPrices.get(index).addToTotal(price);
        }
    }

    public void sort() {
        totalPrices.sort(TotalPrice::compareTo);
    }

    public long getTotalPrice(String type) {
        long total = -1;
        int index = totalPrices.indexOf(new TotalPrice(type));
        if (index >= 0 && index < totalPrices.size()) {
            total = totalPrices.get(index).getTotal();
        }
        return total;
    }

    public void reset() {
        for (TotalPrice p : totalPrices) {
            p.addToTotal(-p.getTotal());
        }
    }

    @Override
    public String toString() {
        String s = "";
        for (TotalPrice p : totalPrices) {
            s += p + System.lineSeparator();
        }
        return s;
    }
}
