package proggestionepatrimoniale;

public class EdificioFactory {
    public static Edificio fromCSVLine(String line) {
        Edificio e;
        String[] campi = line.split(";");

        String via = campi[0];
        String città = campi[1];
        int zip = Integer.parseInt(campi[2]);
        String stato = campi[3];
        int letti = Integer.parseInt(campi[4]);
        int bagni = Integer.parseInt(campi[5]);
        int superficie = Integer.parseInt(campi[6]);
        String dataVendita = campi[8];
        int prezzo = Integer.parseInt(campi[9]);
        double latitudine = Double.parseDouble(campi[10]);
        double longitudine = Double.parseDouble(campi[11]);
        switch(campi[7]) {
            case "Residential": {
                e = new Residenziale(via, città, stato, zip, letti, bagni, superficie, dataVendita, prezzo, latitudine, longitudine);
                break;
            }
            case "Condo": {
                e = new Condominio(via, città, stato, zip, letti, bagni, superficie, dataVendita, prezzo, latitudine, longitudine);
                break;
            }
            case "Unknown": {
                e = new Sconosciuto(via, città, stato, zip, letti, bagni, superficie, dataVendita, prezzo, latitudine, longitudine);
                break;
            }
            case "Multi-Family": {
                e = new Multifamiliare(via, città, stato, zip, letti, bagni, superficie, dataVendita, prezzo, latitudine, longitudine);
                break;
            }
            default: {
                e = null;
                break;
            }
        }
        return e;
    }
}
