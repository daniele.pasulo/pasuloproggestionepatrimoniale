package proggestionepatrimoniale;

import java.io.Serializable;

public abstract class Edificio implements Serializable, Comparable<Edificio> {
    private static final long serialVersionUID = 2L;

    protected String via;
    protected String città;
    protected String stato;
    protected int zip;

    protected int letti;
    protected int bagni;
    protected int superficie;

    protected String tipo;
    protected String dataVendita;
    protected int prezzo;
    protected double latitudine;
    protected double longitudine;


    public Edificio(String via, String città, String stato, int zip, int letti, int bagni, int superficie, String tipo, String dataVendita, int prezzo, double latitudine, double longitudine) {
        this.via = via;
        this.città = città;
        this.stato = stato;
        this.zip = zip;
        this.letti = letti;
        this.bagni = bagni;
        this.superficie = superficie;
        this.tipo = tipo;
        this.dataVendita = dataVendita;
        this.prezzo = prezzo;
        this.latitudine = latitudine;
        this.longitudine = longitudine;
    }

    public Edificio(Edificio e) {
        this(e.via, e.città, e.stato, e.zip, e.letti, e.bagni, e.superficie, e.tipo, e.dataVendita, e.prezzo, e.latitudine, e.longitudine);
    }

    public void setLetti(int letti) {
        this.letti = letti;
    }

    public void setBagni(int bagni) {
        this.bagni = bagni;
    }

    public void setSuperficie(int superficie) {
        this.superficie = superficie;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setDataVendita(String dataVendita) {
        this.dataVendita = dataVendita;
    }

    public void setPrezzo(int prezzo) {
        this.prezzo = prezzo;
    }

    public String getVia() {
        return via;
    }

    public String getCittà() {
        return città;
    }

    public String getStato() {
        return stato;
    }

    public int getZip() {
        return zip;
    }

    public int getLetti() {
        return letti;
    }

    public int getBagni() {
        return bagni;
    }

    public int getSuperficie() {
        return superficie;
    }

    public String getTipo() {
        return tipo;
    }

    public String getDataVendita() {
        return dataVendita;
    }

    public int getPrezzo() {
        return prezzo;
    }

    public double getLatitudine() {
        return latitudine;
    }

    public double getLongitudine() {
        return longitudine;
    }

    @Override
    public int compareTo(Edificio o) {
        return prezzo - o.prezzo;
    }

    @Override
    public boolean equals(Object o) {
        boolean equal = false;
        if (this == o) {
            equal = true;
        } else {
            if (o instanceof Edificio) {
                Edificio e = (Edificio) o;
                equal = e.via.equals(via) && e.città.equals(città) && e.stato.equals(stato) &&
                        e.zip == zip && e.letti == letti && e.bagni == bagni && e.superficie == superficie &&
                        e.tipo.equals(tipo) && e.dataVendita.equals(dataVendita) && e.prezzo == prezzo &&
                        e.latitudine == latitudine && e.longitudine == longitudine;
            }
        }
        return equal;
    }

    @Override
    public String toString() {
        return "Via: " + via + System.lineSeparator() +
                "Città: " + città + System.lineSeparator() +
                "Stato: " + stato + System.lineSeparator() +
                "Zip: " + zip + System.lineSeparator() +
                "Letti: " + letti + System.lineSeparator() +
                "Bagni: " + bagni + System.lineSeparator() +
                "Superficie: " + superficie + System.lineSeparator() +
                "Tipo: " + tipo + System.lineSeparator() +
                "Data di vendita: " + dataVendita + System.lineSeparator() +
                "Prezzo: " + prezzo + System.lineSeparator() +
                "Latitudine: " + latitudine + System.lineSeparator() +
                "Longitudine: " + longitudine + System.lineSeparator();
    }

    @Override
    public abstract Edificio clone();
}
