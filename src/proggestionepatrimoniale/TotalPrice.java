package proggestionepatrimoniale;

import java.math.BigDecimal;

public class TotalPrice implements Comparable<TotalPrice> {
    private final String type;
    private BigDecimal total;

    public TotalPrice(String type) {
        this.type = type;
        total = new BigDecimal(0);
    }

    public void addToTotal(int price) {
        total = total.add(new BigDecimal(price));
    }

    public int getTotal() {
        return total.intValue();
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equal = false;
        if (this == obj) {
            equal = true;
        }
        else if (obj instanceof TotalPrice) {
            equal = ((TotalPrice) obj).type.equals(type);
        }
        return equal;
    }

    @Override
    public String toString() {
        return type+": "+total.intValue();
    }

    @Override
    public int compareTo(TotalPrice o) {
        return total.compareTo(o.total);
    }
}
