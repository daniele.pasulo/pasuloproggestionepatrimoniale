package proggestionepatrimoniale;

import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

import java.util.Scanner;
import java.util.ArrayList;

public class GestionePatrimoniale {

    private TotalPriceXType sommeTotali;
    private ArrayList<Edificio> edifici;

    private boolean modifyFlag;

    public GestionePatrimoniale() {
        sommeTotali = new TotalPriceXType();
        modifyFlag = true;
        edifici = new ArrayList<>();
    }

    public boolean add(Edificio e) {
        modifyFlag = edifici.add(e.clone());
        return modifyFlag;
    }

    public boolean remove(Edificio e) {
        modifyFlag = edifici.removeIf(e::equals);
        return modifyFlag;
    }

    public void loadCSV(String filename) {
        ArrayList<Edificio> edifici = new ArrayList<>();
        try (Scanner s = new Scanner(new File(filename)).useDelimiter(System.lineSeparator())) {
            s.nextLine();
            while (s.hasNext()) {
                edifici.add(EdificioFactory.fromCSVLine(s.nextLine()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.edifici = edifici;
    }

    public String calcolaSommeTotali() {
        if (modifyFlag) {
            sommeTotali.reset();
            for (Edificio e : edifici) {
                String key = e.getTipo();
                sommeTotali.add(key, e.getPrezzo());
            }
            modifyFlag = false;
        }
        return sommeTotali.toString();
    }

    public long getTotal(String type) {
        return sommeTotali.getTotalPrice(type);
    }

    public void stampa() {
        System.out.println(this);
    }

    @SuppressWarnings("unchecked")
    public boolean load(String filename) {
        boolean success;
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))) {
            edifici = (ArrayList<Edificio>) in.readObject();

            success = true;
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    public boolean save(String filename) {
        boolean success;
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))) {
            out.writeObject(edifici);
            success = true;
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }

        return success;
    }

    @Override
    public String toString() {
        String s = "";
        for (Edificio e : edifici) {
            s += e + System.lineSeparator();
        }
        return s;
    }

    public void sort() {
        sommeTotali.sort();
        edifici.sort(Edificio::compareTo);
    }
}