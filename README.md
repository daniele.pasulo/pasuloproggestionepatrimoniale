# Progetto Gestione Patrimoniale

In questo progetto, lo scopo è quello di caricare, da un file CSV,
un elenco di oggetti, sul quale poi si andranno a fare varie operazioni quali:

- Ordinamento in base ad un criterio
- Salvataggio e caricamento in formato binario
- Calcolo totale di ogni categoria
- Trovare un elenco di edifici entro un raggio di n km dato un punto

### Strutturazione dei branch
- **master**: branch principale sul quale è caricata la prima versione del progetto
- **Versione2**: branch su cui è salvata una versione del progetto che implementa il polimorfismo
- **Coordinate**: branch che conterrà la versione del progetto che gestisce le operazioni sulle coordinate
degli oggetti caricati