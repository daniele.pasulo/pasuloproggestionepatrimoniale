package proggestionepatrimoniale;

public class Condominio extends Edificio {

    public Condominio(String via, String città, String stato, int zip, int letti, int bagni, int superficie, String dataVendita, int prezzo, double latitudine, double longitudine) {
        super(via, città, stato, zip, letti, bagni, superficie, "Condo", dataVendita, prezzo, latitudine, longitudine);
    }

    public Condominio(Condominio c) {
        super(c);
    }

    @Override
    public Condominio clone() {
        return new Condominio(this);
    }
}
