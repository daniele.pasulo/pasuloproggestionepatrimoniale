package proggestionepatrimoniale;

public class Residenziale extends Edificio {

    public Residenziale(String via, String città, String stato, int zip, int letti, int bagni, int superficie, String dataVendita, int prezzo, double latitudine, double longitudine) {
        super(via, città, stato, zip, letti, bagni, superficie, "Residential", dataVendita, prezzo, latitudine, longitudine);
    }

    public Residenziale(Residenziale e) {
        super(e);
    }

    @Override
    public Residenziale clone() {
        return new Residenziale(this);
    }
}
